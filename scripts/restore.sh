#!/usr/bin/env bash
###########################################################################################
# Title:         CloudHTPC Restore Service: Restore Script                                #
# URL:           https://gitlab.com/chadporter/cloud-htpc                                 #
# Description:   Restores encrypted config files from CloudHTPC Restore Service.          #
# --                                                                                      #
###########################################################################################
#                     GNU General Public License v3.0                                     #
###########################################################################################
# Usage:                                                                                  #
# ======                                                                                  #
# Simple:                                                                                 #
# curl -s https://cloudhtpc.gitlab.io/scripts/restore.sh | bash -s 'USER' 'PASS'          #
# wget -qO- https://cloudhtpc.gitlab.io/scripts/restore.sh | bash -s 'USER' 'PASS'        #
#                                                                                         #
# Custom CloudHTPC Path:                                                                  #
# curl -s https://cloudhtpc.gitlab.io/scripts/restore.sh | bash -s 'USER' 'PASS' 'PATH'   #
# wget -qO- https://cloudhtpc.gitlab.io/scripts/restore.sh | bash -s 'USER' 'PASS' 'PATH' #
###########################################################################################

# vars
files=( "ansible.cfg" "accounts.yml" "settings.yml" "adv_settings.yml" "backup_config.yml" "rclone.conf" )
restore="restore.cloudhtpc.gitlab.io"
folder="/tmp/restore_service"
green="\e[1;32m"
red="\e[1;31m"
nc="\e[0m"
done="[ ${green}DONE${nc} ]"
fail="[ ${red}FAIL${nc} ]"

# Print banner

echo -e "
$green┌─────────────────────────────────────────────────────────────────────┐
$green│ Title:         CloudHTPC Restore Service: Restore Script            │
$green│ URL:           https://gitlab.com/chadporter/cloud-htpc             │
$green│ Description:   Restores encrypted config files from the             │
$green│                CloudHTPC Restore Service.                           │
$green├─────────────────────────────────────────────────────────────────────┤
$green├─────────────────────────────────────────────────────────────────────┤
$green│                  GNU General Public License v3.0                    │
$green└─────────────────────────────────────────────────────────────────────┘
$nc"


# inputs
USER=$1
PASS=$2
DIR=${3:-$HOME/cloudhtpc}

# validate inputs
if [ -z "$USER" ] || [ -z "$PASS" ]
then
      echo "You must provide the USER & PASS as arguments"
      exit 1
fi

# validate folders exist
TMP_FOLDER_RESULT=$(mkdir -p $folder)
if [ ! -z "$TMP_FOLDER_RESULT" ]
then
    echo "Failed to ensure $folder was created..."
    exit 1
else
   rm -rf $folder/*
fi


RESTORE_FOLDER_RESULT=$(mkdir -p $DIR)
if [ ! -z "$RESTORE_FOLDER_RESULT" ]
then
    echo "Failed to ensure $DIR was created..."
    exit 1
fi

# SHA1 username
USER_HASH=$(echo -n "$USER" | openssl dgst -sha1 | sed 's/^.*= //')
echo "User Hash: $USER_HASH"
echo ''

# Fetch files
echo "Fetching files from $restore..."
echo ''
for file in "${files[@]}"
do
        :
        # wget file
        printf '%-20.20s' "$file"
        wget -qO $folder/$file.enc http://$restore/load/$USER_HASH/$file
        file_header=$(head -c 10 $folder/$file.enc)
        # is the file encrypted?
        if [[ $file_header == Salted* ]]
        then
                echo -e $done
        else
                echo -e $fail
                exit 1
        fi
done

echo ''

# Decrypt files
echo 'Decrypting fetched files...'
echo ''
for file in "${files[@]}"
do
        :
        # wget file
        printf '%-20.20s' "$file"
        DECRYPT_RESULT=$(openssl enc -aes-256-cbc -d -salt -md md5 -in $folder/$file.enc -out $folder/$file -k "$PASS" 2>&1)
        # was the file decryption successful?
        if [ -z "$DECRYPT_RESULT" ]
        then
                echo -e $done
        else
                echo -e $fail
                exit 1
        fi
done

echo ''

# Move decrypted files
echo 'Moving decrypted files...'
echo ''
for file in "${files[@]}"
do
        :
        # move file
        printf '%-20.20s' "$file"
        MOVE_RESULT=$(mv --backup=numbered $folder/$file $DIR/$file 2>&1)
        # was the decrypted file moved successfully?
        if [ -z "$MOVE_RESULT" ]
        then
                echo -e $done
        else
                echo -e $fail
                exit 1
        fi
done

echo ''

# finish
exit 0