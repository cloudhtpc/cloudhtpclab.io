#!/bin/bash
#################################################################################
# Title:         CloudHTPC: CloudHTPC Repo Cloner                               #
# URL:           https://gitlab.com/chadporter/cloud-htpc                       #
# Description:   Clones CloudHTPC repo.                                         #
# --                                                                            #
#################################################################################
#                     GNU General Public License v3.0                           #
#################################################################################
# Usage:                                                                        #
# ======                                                                        #
# curl -s https://cloudhtpc.gitlab.io/scripts/repo.sh | bash                    #
# wget -qO- https://cloudhtpc.gitlab.io/scripts/repo.sh | bash                  #
#################################################################################


## Variables
CLOUDHTPC_PATH="$HOME/cloudhtpc"
CLOUDHTPC_REPO="https://gitlab.com/chadporter/CloudHTPC.git"

## Clone CloudHTPC and pull latest commit
if [ -d "$CLOUDHTPC_PATH" ]; then
    if [ -d "$CLOUDHTPC_PATH/.git" ]; then
        cd "$CLOUDHTPC_PATH"
        git clean -df
        git pull
        git reset --hard origin/$(git rev-parse --abbrev-ref HEAD)
        git submodule update --init --recursive
    else
        cd "$CLOUDHTPC_PATH"
        git init
        git remote add origin "$CLOUDHTPC_REPO"
        git fetch
        git branch master origin/master
        git checkout -f master
        git clean -df
        git pull
        git reset --hard origin/master
        git submodule update --init --recursive
    fi
else
    git clone "$CLOUDHTPC_REPO" "$CLOUDHTPC_PATH"
    cd "$CLOUDHTPC_PATH"
    git submodule update --init --recursive
fi

## Copy settings and config files into CloudHTPC folder
shopt -s nullglob
for i in "$CLOUDHTPC_PATH"/defaults/*.default; do
    if [ ! -f "$CLOUDHTPC_PATH/$(basename "${i%.*}")" ]; then
        cp -n "${i}" "$CLOUDHTPC_PATH/$(basename "${i%.*}")"
    fi
done
shopt -u nullglob

## Set vim as default editor
export EDITOR=vim
if [[ "$SHELL" == *"bash"* ]]; then
    if [ -f $HOME/.bashrc ]; then
        sed -i '/^[ \t]*export EDITOR=/{h;s/=.*/=vim/};${x;/^$/{s//export EDITOR=vim/;H};x}' $HOME/.bashrc
    elif [ -f /etc/skel/.bashrc ]; then
        cp /etc/skel/.bashrc $HOME/.bashrc
        chown $USERNAME:$USERNAME $HOME/.bashrc
        chmod 644 $HOME/.bashrc
        sed -i '/^[ \t]*export EDITOR=/{h;s/=.*/=vim/};${x;/^$/{s//export EDITOR=vim/;H};x}' $HOME/.bashrc
    fi
elif [[ "$SHELL" == *"zsh"* ]]; then
    if [ -f $HOME/.zshrc ]; then
        sed -i '/^[ \t]*export EDITOR=/{h;s/=.*/=vim/};${x;/^$/{s//export EDITOR=vim/;H};x}' $HOME/.zshrc
    fi
fi